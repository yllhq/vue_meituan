import {createApp} from 'vue';
import App from './App.vue';
import {Button,Icon} from 'vant';
import 'amfe-flexible';
import router from "../src/router";
import "./common/css/base.less"

const app = createApp(App)
app.use(Button)
app.use(router)
app.use(Icon)
app.mount('#app')
