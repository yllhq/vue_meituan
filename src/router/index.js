import {createRouter, createWebHashHistory} from "vue-router";

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            redirect: '/home',
        },
        {
            path: '/home',
            component: () => import('../pages/myHome/MyHome'),
        },
        {
            path: '/cart',
            component: () => import('../pages/myCart/MyCart'),
        },
         {
             path: '/mine',
             component: () => import('../pages/mine/Mine.vue'),
         },
        {
            path: '/order',
            component: () => import('../pages/myOrder/MyOrder'),
        },
    ],
});
export default router;