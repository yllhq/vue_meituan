# meituan

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# 说明
vue仿美团项目

# 环境依赖
* npm install postcss-pxtorem@5.1.1 amfe-flexible -S
* npm i less less-loader@7 -S
* npm i vant@next -S
* npm i babel-plugin-import -S