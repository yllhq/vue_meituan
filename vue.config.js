module.exports = {
  lintOnSave: false,
  css: {
    loaderOptions: {
      postcss: {
       postcssOptions:{
        plugins: [
          require('postcss-pxtorem')({ rootValue: 16 , propList: ['*']}),
        ],
       }
      },
    },
  },
};